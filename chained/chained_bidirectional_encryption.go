package chained

import (
	"angel/encoding"
	"angel/utility"
	"errors"
	"fmt"
)

// Struct that defines a chained encryption routine. This allows for specification
// of multiple keys and multiple routines for encryption. This type of shuffling is
// something that will allow you to store and use multiple keys in order to better
// secure data if you feel that a single pass of encryption is simply not enough for
// your application.
type ChainedBidirectionalEncryption struct {
	EncryptionRoutines []utility.BidirectionalEncryption
}

// Validates the number of keys against the set number of encryption routines.
func (c *ChainedBidirectionalEncryption) validateKeyCount(keys [][]byte) error {
	// validate the routine count
	routineCount := 0
	if c.EncryptionRoutines != nil {
		routineCount = len(c.EncryptionRoutines)
	}

	if routineCount == 0 {
		return errors.New("no encryption routines have been specified")
	}

	// validate the keys
	keyCount := 0
	if keys != nil {
		keyCount = len(keys)
	}

	if keyCount != routineCount {
		return errors.New(fmt.Sprintf("key count (%d) does not match routine count (%d)", keyCount, routineCount))
	}

	// if we got here, all is happy
	return nil
}

// Encrypts the given data given all keys in order.
func (c *ChainedBidirectionalEncryption) Encrypt(keys [][]byte, data []byte) ([]byte, error) {
	// validate the key count
	err := c.validateKeyCount(keys)
	if err != nil {
		return nil, err
	}

	// execute the encryption one-by-one
	lastResult := make([]byte, len(data))
	copy(lastResult, data)
	for index, routine := range c.EncryptionRoutines {
		lastResult, err = routine.Encrypt(keys[index], lastResult)
		if err != nil {
			return nil, err
		}
	}

	// return the last result
	return lastResult, nil
}

// Encrypts the given data and then encodes it using the given encoder.
func (c *ChainedBidirectionalEncryption) EncryptAndEncode(keys [][]byte, data []byte, encode utility.EncodeBytesToString) (*string, error) {
	result, err := c.Encrypt(keys, data)
	if err != nil {
		return nil, err
	}
	return encode(result)
}

// Encrypts the given data and then encodes it as Base 64.
func (c *ChainedBidirectionalEncryption) EncryptBase64(keys[][] byte, data []byte) (*string, error) {
	return c.EncryptAndEncode(keys, data, encoding.EncodeBase64)
}

// Encrypts the given data and then encodes it as hex.
func (c *ChainedBidirectionalEncryption) EncryptHex(keys[][] byte, data []byte) (*string, error) {
	return c.EncryptAndEncode(keys, data, encoding.EncodeHex)
}

// Decrypts the given data given all keys in order.
func (c *ChainedBidirectionalEncryption) Decrypt(keys[][]byte, data[] byte) ([]byte, error) {
	// validate the key count
	err := c.validateKeyCount(keys)
	if err != nil {
		return nil, err
	}

	// execute the decryption one-by-one
	lastResult := make([]byte, len(data))
	copy(lastResult, data)
	for index := len(c.EncryptionRoutines) - 1; index >= 0; index-- {
		lastResult, err = c.EncryptionRoutines[index].Decrypt(keys[index], lastResult)
		if err != nil {
			return nil, err
		}
	}

	// return the last result
	return lastResult, nil
}

// Decrypts the given data from the provided encoded string.
func (c *ChainedBidirectionalEncryption) DecryptFromEncoded(keys [][]byte, data string, decode utility.DecodeBytesFromString) ([]byte, error) {
	decoded, err := decode(data)
	if err != nil {
		return nil, err
	}
	return c.Decrypt(keys, decoded)
}

// Decrypts data from a Base 64-encoded string.
func (c *ChainedBidirectionalEncryption) DecryptBase64(keys [][]byte, data string) ([]byte, error) {
	return c.DecryptFromEncoded(keys, data, encoding.DecodeBase64)
}

// Decrypts data from a hex-encoded string.
func (c *ChainedBidirectionalEncryption) DecryptHex(keys [][]byte, data string) ([]byte, error) {
	return c.DecryptFromEncoded(keys, data, encoding.DecodeHex)
}
