package chained

import (
	"angel/aes"
	"angel/random"
	"angel/utility"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

// Tests basic encryption of data using chained execution. Decryption of the data is
// tested separately in the decryption routine. This just checks for errors in the
// core routine for actual encryption.
func TestChainedBidirectionalEncryption_Encrypt(t *testing.T) {
	assertT := assert.New(t)

	for index := 1; index <= 25; index++ {
		keys, err := generateKeys(index)
		assertT.Nil(err, "Error generating keys")

		toEncrypt := random.String(index * 16)

		chain := generateChain(index)
		result, err := chain.Encrypt(keys, []byte(toEncrypt))
		assertT.Nil(err, "Error encountered during encryption")
		assertT.NotNil(result, "Generated a nil result (!?)")
	}
}

// Tests decryption of data in a chain.
func TestChainedBidirectionalEncryption_Decrypt(t *testing.T) {
	assertT := assert.New(t)

	for index := 1; index <= 25; index++ {
		keys, err := generateKeys(index)
		assertT.Nil(err, "Error generating keys")

		toEncrypt := random.String(index * 16)

		chain := generateChain(index)
		encrypted, err := chain.Encrypt(keys, []byte(toEncrypt))
		assertT.Nil(err, "Error encountered during encryption")

		decrypted, err := chain.Decrypt(keys, encrypted)
		assertT.Nil(err, "Error during decryption")
		assertT.Equal([]byte(toEncrypt), decrypted, "Decryption of data failed")
	}
}

// Generates the given number of keys
func generateKeys(count int) ([][]byte, error) {
	var keys [][]byte
	for index := 0; index < count; index++ {
		key, err := aes.GenerateKey(16)
		if err != nil {
			return nil, err
		}
		keys = append(keys, key)
	}
	return keys, nil
}

// Generates a randomized ChainedBidirectionalEncryption instance.
func generateChain(count int) *ChainedBidirectionalEncryption {
	candidates := []utility.BidirectionalEncryption{
		{Encrypt: aes.CBCPKCS5Encrypt, Decrypt: aes.CBCPKCS5Decrypt},
		{Encrypt: aes.CBCPKCS7Encrypt, Decrypt: aes.CBCPKCS7Decrypt},
	}

	localRandom := rand.New(rand.NewSource(time.Now().UTC().Unix()))

	var chainedAlgorithms []utility.BidirectionalEncryption
	for index := 0; index < count; index++ {
		chainedAlgorithms = append(chainedAlgorithms, candidates[localRandom.Intn(len(candidates))])
	}

	return &ChainedBidirectionalEncryption{EncryptionRoutines: chainedAlgorithms}
}
