package blowfish

import (
	"encoding/base64"
	"encoding/hex"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Tests basic key generation.
func TestGenerateKey(t *testing.T) {
	assertT := assert.New(t)

	for index := MinKeySize; index <= MaxKeySize; index += 1 {
		key, err := GenerateKey(index)
		assertT.Nil(err, "Error when generating key")
		assertT.NotNil(key, "Generated a nil key somehow (!?)")
		assertT.Equal(index, len(key), "Generate a key with the wrong length")
	}
}

// Tests the generation of keys encoded in Base 64.
func TestGenerateKeyBase64(t *testing.T) {
	assertT := assert.New(t)

	var decoded []byte
	for index := MinKeySize; index <= MaxKeySize; index += 1 {
		key, err := GenerateKeyBase64(index)
		assertT.Nil(err, "Error when generating key")
		assertT.NotNil(key, "Generated a nil key somehow (!?)")

		decoded, err = base64.StdEncoding.DecodeString(*key)
		assertT.Nil(err, "Error when decoding generated string")
		assertT.Equal(index, len(decoded), "Generate a key with the wrong length after decoding")
	}
}

// Tests the generation of keys encoded in hex.
func TestGenerateKeyHex(t *testing.T) {
	assertT := assert.New(t)

	var decoded []byte
	for index := MinKeySize; index <= MaxKeySize; index += 1 {
		key, err := GenerateKeyHex(index)
		assertT.Nil(err, "Error when generating key")
		assertT.NotNil(key, "Generated a nil key somehow (!?)")

		decoded, err = hex.DecodeString(*key)
		assertT.Nil(err, "Error when decoding generated string")
		assertT.Equal(index, len(decoded), "Generate a key with the wrong length after decoding")
	}
}

// Tests restoration of keys from Base 64.
func TestKeyFromBase64(t *testing.T) {
	assertT := assert.New(t)

	var encoded string
	var decoded []byte

	for index := MinKeySize; index <= MaxKeySize; index += 1 {
		key, err := GenerateKey(index)
		assertT.Nil(err, "Error when generating key")
		assertT.NotNil(key, "Generated a nil key somehow (!?)")

		encoded = base64.StdEncoding.EncodeToString(key)
		decoded, err = KeyFromBase64(encoded)
		assertT.Nil(err, "Error when decoding encoded key")
		assertT.Equal(key, decoded, "Decoded the key improperly")
	}
}

// Tests restoration of keys from hex.
func TestKeyFromHex(t *testing.T) {
	assertT := assert.New(t)

	var encoded string
	var decoded []byte

	for index := MinKeySize; index <= MaxKeySize; index += 1 {
		key, err := GenerateKey(index)
		assertT.Nil(err, "Error when generating key")
		assertT.NotNil(key, "Generated a nil key somehow (!?)")

		encoded = hex.EncodeToString(key)
		decoded, err = KeyFromHex(encoded)
		assertT.Nil(err, "Error when decoding encoded key")
		assertT.Equal(key, decoded, "Decoded the key improperly")
	}
}
