package blowfish

import (
	"angel/random"
	"github.com/stretchr/testify/assert"
	"testing"
)

const MaxRandomStringSize = 1024

// Tests basic encrypt using Blowfish. This really just ensures that we don't get
// any errors. The decryption routine is where you'll find the testing of the
// back-and-forth of the encryption.
func TestCBCEncrypt(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index <= MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted1, err := CBCEncrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted1, "Generated a nil encryption byte slice (!?)")

		encrypted2, err := CBCEncrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted2, "Generated a nil encryption byte slice (!?)")

		assertT.NotEqual(encrypted1, encrypted2, "Seeding must be bad, we generated the same encryption twice")
	}
}

// Tests decryption using Blowfish.
func TestCBCDecrypt(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error generating key")

	var testString string
	for index := 1; index <= MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCEncrypt(encryptionKey, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCDecrypt(encryptionKey, encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by Base 64 encoding.
func TestCBCEncryptBase64(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index < MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCEncryptBase64(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption Base 64 string (!?)")
	}
}

// Tests decryption from Base 64-encoded data.
func TestCBCDecryptBase64(t *testing.T) {
	assertT := assert.New(t)

	key, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index < MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCEncryptBase64(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCDecryptBase64(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by hex encoding.
func TestCBCEncryptHex(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index < MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCEncryptHex(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption hex string (!?)")
	}
}

// Tests decryption from hex-encoded data.
func TestCBCDecryptHex(t *testing.T) {
	assertT := assert.New(t)

	key, err := GenerateKey(MaxKeySize)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index < MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCEncryptHex(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCDecryptHex(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}
