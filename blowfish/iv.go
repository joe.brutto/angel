package blowfish

import (
	"crypto/rand"
	fish "golang.org/x/crypto/blowfish"
)

// Generates an IV of the given byte size.
func GenerateIV() ([]byte, error) {
	data := make([]byte, fish.BlockSize)
	_, err := rand.Read(data)
	return data, err
}
