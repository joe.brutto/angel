package blowfish

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Tests the basic routine for generation of IV values.
func TestGenerateIV(t *testing.T) {
	assertT := assert.New(t)

	iv, err := GenerateIV()
	assertT.Nil(err, "Error generating IV value")
	assertT.NotNil(iv, "Generated a nil IV value somehow (!?)")
}
