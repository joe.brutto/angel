module angel

go 1.12

require (
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
)
