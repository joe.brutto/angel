package aes

import (
	"angel/random"
	"encoding/base64"
	"github.com/stretchr/testify/assert"
	"testing"
)

const OtherSourcePkcs7CbcEncrypted = "xRYpkkASnxJ4/1LbdNQBW3F/3677thY6YS/7Vg5pwKQ="

// Tests basic encrypt using AES/CBC/PKCS7Padding. This really just ensures that we
// don't get any errors. The decryption routine is where you'll find the testing of
// the back-and-forth of the encryption.
func TestCBCPKCS7Encrypt(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index <= MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted1, err := CBCPKCS7Encrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted1, "Generated a nil encryption byte slice (!?)")

		encrypted2, err := CBCPKCS7Encrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted2, "Generated a nil encryption byte slice (!?)")

		assertT.NotEqual(encrypted1, encrypted2, "Seeding must be bad, we generated the same encryption twice")
	}
}

// Tests decryption using AES/CBC/PKCS7Padding.
func TestCBCPKCS7Decrypt(t *testing.T) {
	assertT := assert.New(t)

	// use some known data generated from another source
	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding key from Base 64")

	data, err := base64.StdEncoding.DecodeString(OtherSourcePkcs7CbcEncrypted)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	decrypted, err := CBCPKCS7Decrypt(key, data)
	assertT.Nil(err, "Error decrypting the data")
	assertT.Equal(OtherSourceExpected, string(decrypted), "Invalid string decrypted")

	var testString string
	for index := 1; index <= MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS7Encrypt(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS7Decrypt(key, encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by Base 64 encoding.
func TestCBCPKCS7EncryptBase64(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index <= MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCPKCS7EncryptBase64(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption Base 64 string (!?)")
	}
}

// Tests decryption from Base 64-encoded data.
func TestCBCPKCS7DecryptBase64(t *testing.T) {
	assertT := assert.New(t)

	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index <= MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS7EncryptBase64(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS7DecryptBase64(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by hex encoding.
func TestCBCPKCS7EncryptHex(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index <= MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCPKCS7EncryptHex(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption hex string (!?)")
	}
}

// Tests decryption from hex-encoded data.
func TestCBCPKCS7DecryptHex(t *testing.T) {
	assertT := assert.New(t)

	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index <= MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS7EncryptHex(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS7DecryptHex(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}
