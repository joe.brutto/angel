package aes

import (
	"angel/random"
	"encoding/base64"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	MaxRandomStringSize = 1024

	OtherSourceKey               = "UcgBasnre0DC1u0XyZYIdMcvWwBlFRNMEle4oCmPUnU="
	OtherSourceExpected          = "Don't Panic!"
	OtherSourcePkcs5CbcEncrypted = "K+Cy9sN+qA4At81WgrbdmCwQhbDdiiLEeg9wN3pSL8w="
)

// Tests basic encrypt using AES/CBC/PKCS5Padding. This really just ensures that we
// don't get any errors. The decryption routine is where you'll find the testing of
// the back-and-forth of the encryption.
func TestCBCPKCS5Encrypt(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index < MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted1, err := CBCPKCS5Encrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted1, "Generated a nil encryption byte slice (!?)")

		encrypted2, err := CBCPKCS5Encrypt(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted2, "Generated a nil encryption byte slice (!?)")

		assertT.NotEqual(encrypted1, encrypted2, "Seeding must be bad, we generated the same encryption twice")
	}
}

// Tests decryption using AES/CBC/PKCS5Padding.
func TestCBCPKCS5Decrypt(t *testing.T) {
	assertT := assert.New(t)

	// use some known data generated from another source
	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding key from Base 64")

	data, err := base64.StdEncoding.DecodeString(OtherSourcePkcs5CbcEncrypted)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	decrypted, err := CBCPKCS5Decrypt(key, data)
	assertT.Nil(err, "Error decrypting the data")
	assertT.Equal(OtherSourceExpected, string(decrypted), "Invalid string decrypted")

	var testString string
	for index := 1; index < MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS5Encrypt(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS5Decrypt(key, encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by Base 64 encoding.
func TestCBCPKCS5EncryptBase64(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index < MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCPKCS5EncryptBase64(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption Base 64 string (!?)")
	}
}

// Tests decryption from Base 64-encoded data.
func TestCBCPKCS5DecryptBase64(t *testing.T) {
	assertT := assert.New(t)

	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index < MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS5EncryptBase64(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS5DecryptBase64(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}

// Tests encryption followed by hex encoding.
func TestCBCPKCS5EncryptHex(t *testing.T) {
	assertT := assert.New(t)

	encryptionKey, err := GenerateKey(DefaultKeySize)
	assertT.Nil(err, "Error generating key")

	var toEncrypt string
	for index := 1; index < MaxRandomStringSize; index++ {
		toEncrypt = random.String(index)

		encrypted, err := CBCPKCS5EncryptHex(encryptionKey, []byte(toEncrypt))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption hex string (!?)")
	}
}

// Tests decryption from hex-encoded data.
func TestCBCPKCS5DecryptHex(t *testing.T) {
	assertT := assert.New(t)

	key, err := KeyFromBase64(OtherSourceKey)
	assertT.Nil(err, "Error decoding encrypted data from Base 64")

	var testString string
	for index := 1; index < MaxRandomStringSize; index++ {
		testString = random.String(index)

		encrypted, err := CBCPKCS5EncryptHex(key, []byte(testString))
		assertT.Nil(err, "Error encrypting data")

		decrypted, err := CBCPKCS5DecryptHex(key, *encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated nil decrypted data (!?)")
		assertT.Equal(testString, string(decrypted), "Decrypted the data incorrectly")
	}
}
