package aes

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Tests the basic routine for generation of IV values.
func TestGenerateIV(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index <= 4096; index *= 2 {
		iv, err := GenerateIV(index)
		assertT.Nil(err, "Error generating IV value")
		assertT.NotNil(iv, "Generated a nil IV value somehow (!?)")
		assertT.Equal(index, len(iv), "Generated a key of an invalid length")
	}
}

// Tests generation of IV data of the default size.
func TestGenerateDefaultIv(t *testing.T) {
	assertT := assert.New(t)

	iv, err := GenerateDefaultIv()
	assertT.Nil(err, "Error generating IV value")
	assertT.NotNil(iv, "Generated a nil IV value somehow (!?)")
	assertT.Equal(DefaultIVSize, len(iv), "Generated a key of an invalid length")
}
