package aes

import (
	"angel/encoding"
	"angel/utility"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"errors"
)

// Executes the encryption routine for the AES/CBC/PKCS7PADDING combination. This
// assumes that we want the IV as the first 16 bytes of the encrypted data. Any
// errors we encounter are returned unless there's some kind of weird panic under
// the covers that we didn't expect to encounter.
func CBCPKCS7Encrypt(key []byte, data []byte) ([]byte, error) {
	// IV generation
	iv, err := GenerateDefaultIv()
	if err != nil {
		return nil, err
	}

	// encrypt
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	paddedContent := pkcs7Padding(data, block.BlockSize())

	encrypted := make([]byte, len(paddedContent))
	ecb := cipher.NewCBCEncrypter(block, iv)
	ecb.CryptBlocks(encrypted, paddedContent)

	// shove the IV on the front of the byte array & exit
	return append(iv, encrypted...), nil
}

// Executes the decryption routine for the AES/CBC/PKCS7PADDING combination. This
// assumes that we have the IV as the first 16 bytes of the encrypted data. Any
// errors we encounter are returned unless there's some kind of weird panic under
// the covers that we didn't expect to encounter.
func CBCPKCS7Decrypt(key []byte, data []byte) ([]byte, error) {
	// segregate the data
	ivBytes := data[:16]
	toDecryptByes := data[16:]

	// decrypt
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	decrypted := make([]byte, len(toDecryptByes))
	ecb := cipher.NewCBCDecrypter(block, ivBytes)
	ecb.CryptBlocks(decrypted, toDecryptByes)

	// trim & exit
	return pkcs7Trimming(decrypted, aes.BlockSize)
}

// Executes the encryption routine for the AES/CBC/PKCS7PADDING combination and then
// encodes it using the given routine.
func CBCPKCS7EncryptEncoded(key []byte, data []byte, encode utility.EncodeBytesToString) (*string, error) {
	return utility.ExecuteWithEncoding(key, data, CBCPKCS7Encrypt, encode)
}

// Executes the encryption routine for the AES/CBC/PKCS7PADDING combination assuming
// that the data conforms to the given encoding.
func CBCPKCS7DecryptEncoded(key []byte, data string, decode utility.DecodeBytesFromString) ([]byte, error) {
	return utility.ExecuteWithDecoding(key, data, CBCPKCS7Decrypt, decode)
}

// Executes the encryption routine for the AES/CBC/PKCS7PADDING combination and then
// Base 64-encodes the result. This assumes that we want the IV as the first bytes
// of the encrypted data.
func CBCPKCS7EncryptBase64(key []byte, data []byte) (*string, error) {
	return CBCPKCS7EncryptEncoded(key, data, encoding.EncodeBase64)
}

// Executes the decryption routine for the AES/CBC/PKCS7PADDING combination assuming
// that the incoming data is Base 64-encoded. This also assumes that we have the IV
// as the first bytes of the encrypted data.
func CBCPKCS7DecryptBase64(key []byte, data string) ([]byte, error) {
	return CBCPKCS7DecryptEncoded(key, data, encoding.DecodeBase64)
}

// Executes the encryption routine for the AES/CBC/PKCS7PADDING combination and then
// hex-encodes the result. This assumes that we want the IV as the first bytes of
// the encrypted data.
func CBCPKCS7EncryptHex(key []byte, data []byte) (*string, error) {
	return CBCPKCS7EncryptEncoded(key, data, encoding.EncodeHex)
}

// Executes the decryption routine for the AES/CBC/PKCS7PADDING combination assuming
// that the incoming data is hex-encoded. This also assumes that we have the IV as
// the first bytes of the encrypted data.
func CBCPKCS7DecryptHex(key []byte, data string) ([]byte, error) {
	return CBCPKCS7DecryptEncoded(key, data, encoding.DecodeHex)
}

// Performs PKCS7 padding against the provided data.
func pkcs7Padding(data []byte, blockSize int) []byte {
	length := blockSize - (len(data) % blockSize)
	paddedBytes := make([]byte, len(data) + length)

	copy(paddedBytes, data)
	copy(paddedBytes[len(data):], bytes.Repeat([]byte{byte(length)}, length))

	return paddedBytes
}

// Performs PKCS7 trimming against the provided data.
func pkcs7Trimming(data[] byte, blockSize int) ([]byte, error) {
	// simple sanity check
	if len(data) % blockSize != 0 {
		return nil, errors.New("invalid PKCS7 padding")
	}

	// trim & exit
	sign := data[len(data) - 1]
	return data[:len(data) - int(sign)], nil
}
