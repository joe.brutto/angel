package aes

import (
	"angel/encoding"
	"angel/utility"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
)

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination. This
// assumes that we want the IV as the first 16 bytes of the encrypted data. Any
// errors we encounter are returned unless there's some kind of weird panic under
// the covers that we didn't expect to run across.
func CBCPKCS5Encrypt(key []byte, data []byte) ([]byte, error) {
	// IV generation
	iv, err := GenerateDefaultIv()
	if err != nil {
		return nil, err
	}

	// encrypt
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	paddedContent := pkcs5Padding(data, block.BlockSize())

	encrypted := make([]byte, len(paddedContent))
	ecb := cipher.NewCBCEncrypter(block, iv)
	ecb.CryptBlocks(encrypted, paddedContent)

	// shove the IV on the front of the byte array & exit
	return append(iv, encrypted...), nil
}

// Executes the decryption routine for the AES/CBC/PKCS5PADDING combination. This
// assumes that the IV is the first 16 bytes of the encrypted data. Any errors we
// encounter are returned unless there's some kind of weird panic under the covers
// that we didn't expect to run across.
func CBCPKCS5Decrypt(key []byte, data []byte) ([]byte, error) {
	// segregate the data
	ivBytes := data[:16]
	toDecryptByes := data[16:]

	// decrypt
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	decrypted := make([]byte, len(toDecryptByes))
	ecb := cipher.NewCBCDecrypter(block, ivBytes)
	ecb.CryptBlocks(decrypted, toDecryptByes)

	// trim & exit
	return pkcs5Trimming(decrypted), nil
}

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination and then
// encodes it using the given routine.
func CBCPKCS5EncryptEncoded(key []byte, data []byte, encode utility.EncodeBytesToString) (*string, error) {
	return utility.ExecuteWithEncoding(key, data, CBCPKCS5Encrypt, encode)
}

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination assuming
// that the data conforms to the given encoding.
func CBCPKCS5DecryptEncoded(key []byte, data string, decode utility.DecodeBytesFromString) ([]byte, error) {
	return utility.ExecuteWithDecoding(key, data, CBCPKCS5Decrypt, decode)
}

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination and then
// Base 64-encodes the result. This assumes that we want the IV as the first bytes
// of the encrypted data.
func CBCPKCS5EncryptBase64(key []byte, data []byte) (*string, error) {
	return CBCPKCS5EncryptEncoded(key, data, encoding.EncodeBase64)
}

// Executes the decryption routine for the AES/CBC/PKCS5PADDING combination assuming
// that the incoming data is Base 64-encoded. This also assumes that we have the IV
// as the first bytes of the encrypted data.
func CBCPKCS5DecryptBase64(key []byte, data string) ([]byte, error) {
	return CBCPKCS5DecryptEncoded(key, data, encoding.DecodeBase64)
}

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination and then
// hex-encodes the result. This assumes that we want the IV as the first bytes of
// the encrypted data.
func CBCPKCS5EncryptHex(key []byte, data []byte) (*string, error) {
	return CBCPKCS5EncryptEncoded(key, data, encoding.EncodeHex)
}

// Executes the decryption routine for the AES/CBC/PKCS5PADDING combination assuming
// that the incoming data is hex-encoded. This also assumes that we have the IV as
// the first bytes of the encrypted data.
func CBCPKCS5DecryptHex(key []byte, data string) ([]byte, error) {
	return CBCPKCS5DecryptEncoded(key, data, encoding.DecodeHex)
}

// Performs PKCS5 padding against the provided data.
func pkcs5Padding(data []byte, blockSize int) []byte {
	padding := blockSize - len(data) % blockSize
	paddingText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, paddingText...)
}

// Performs PKCS5 trimming against the provided data.
func pkcs5Trimming(data []byte) []byte {
	padding := data[len(data) - 1]
	return data[:len(data) - int(padding)]
}
