package aes

import "crypto/rand"

const DefaultIVSize = 16

// Generates an IV of the given byte size.
func GenerateIV(size int) ([]byte, error) {
	data := make([]byte, size)
	_, err := rand.Read(data)
	return data, err
}

// Generates an IV of the default byte size (16).
func GenerateDefaultIv() ([]byte, error) {
	return GenerateIV(DefaultIVSize)
}
