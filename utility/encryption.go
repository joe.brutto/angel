package utility

// Definition the matches the definitions of our basic encryption routines.
type DirectionalEncryption func(key []byte, data []byte) ([]byte, error)

// Definition for a pair of encryption/decryption calls.
type BidirectionalEncryption struct {
	Encrypt DirectionalEncryption
	Decrypt DirectionalEncryption
}

// Definition for byte data input and encoding as a string.
type EncodeBytesToString func(input []byte) (*string, error)

// Definition for taking string data and decoding it to bytes.
type DecodeBytesFromString func(input string) ([]byte, error)

// Executes encryption/decryption and then encodes the data using the provided
// routines. This provides the ability to write things in shorthand and reduce any
// boilerplate code that is otherwise required.
func ExecuteWithEncoding(key []byte, data []byte, encryption DirectionalEncryption, encoding EncodeBytesToString) (*string, error) {
	result, err := encryption(key, data)
	if err != nil {
		return nil, err
	}

	return encoding(result)
}

// Executes encryption/decryption after decoding the data using the provided
// routines. This provides the ability to write things in shorthand and reduce any
// boilerplate code that is otherwise required.
func ExecuteWithDecoding(key []byte, data string, encryption DirectionalEncryption, decoding DecodeBytesFromString) ([]byte, error) {
	decodedBytes, err := decoding(data)
	if err != nil {
		return nil, err
	}

	return encryption(key, decodedBytes)
}
