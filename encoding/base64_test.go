package encoding

import (
	"angel/random"
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

const Base64Regex = `^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$`

// Tests the encoding of data to Base 64.
func TestEncodeBase64(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index < 4096; index *= 2 {
		toEncode, err := random.Bytes(index)
		assertT.Nil(err, "Error generating random byte sequence")

		encoded, err := EncodeBase64(toEncode)
		assertT.Nil(err, "Error encoding to Base 64")

		matches, err := regexp.MatchString(Base64Regex, *encoded)
		assertT.Nil(err, "Error attempting Base 64 matching")
		assertT.Truef(matches, "Generated string does not appear to be valid Base 64: %s", *encoded)
	}
}

// Tests the decoding of Base 64 data.
func TestDecodeBase64(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index < 4096; index *= 2 {
		toEncode, err := random.Bytes(index)
		assertT.Nil(err, "Error generating random byte sequence")

		encoded, err := EncodeBase64(toEncode)
		assertT.Nil(err, "Error encoding to Base 64")

		decoded, err := DecodeBase64(*encoded)
		assertT.Nil(err, "Error decoding Base 64 data")
		assertT.Equal(toEncode, decoded, "Invalid data decoded")
	}
}

