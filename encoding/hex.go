package encoding

import (
	"encoding/hex"
)

// Encoding routine for Base 64 matching internal standards.
func EncodeHex(input []byte) (*string, error) {
	encoded := hex.EncodeToString(input)
	return &encoded, nil
}

// Decoding routine for Base 64 matching internal standards.
func DecodeHex(input string) ([]byte, error) {
	return hex.DecodeString(input)
}
