package encoding

import "encoding/base64"

// Encoding routine for Base 64 matching internal standards.
func EncodeBase64(input []byte) (*string, error) {
	encoded := base64.StdEncoding.EncodeToString(input)
	return &encoded, nil
}

// Decoding routine for Base 64 matching internal standards.
func DecodeBase64(input string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(input)
}
