package encoding

import (
	"angel/random"
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

const HexRegex = `(?i)^[a-f0-9]+$`

// Tests the encoding of data to hex.
func TestEncodeHex(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index < 4096; index *= 2 {
		toEncode, err := random.Bytes(index)
		assertT.Nil(err, "Error generating random byte sequence")

		encoded, err := EncodeHex(toEncode)
		assertT.Nil(err, "Error encoding to hex")

		matches, err := regexp.MatchString(HexRegex, *encoded)
		assertT.Nil(err, "Error attempting hex matching")
		assertT.Truef(matches, "Generated string does not appear to be valid hex: %s", *encoded)
	}
}

// Tests the decoding of hex data.
func TestDecodeHex(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index < 4096; index *= 2 {
		toEncode, err := random.Bytes(index)
		assertT.Nil(err, "Error generating random byte sequence")

		encoded, err := EncodeHex(toEncode)
		assertT.Nil(err, "Error encoding to hex")

		decoded, err := DecodeHex(*encoded)
		assertT.Nil(err, "Error decoding hex data")
		assertT.Equal(toEncode, decoded, "Invalid data decoded")
	}
}
