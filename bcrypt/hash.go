package bcrypt

import (
	"encoding/base64"
	"encoding/hex"
	bee "golang.org/x/crypto/bcrypt"
)

// Hashes a piece of data with a salt (using the default bcrypt cost).
func Hash(data []byte) ([]byte, error) {
	return HashWithCost(data, bee.DefaultCost)
}

// Hashes a piece of data with a salt and given cost.
func HashWithCost(data []byte, cost int) ([]byte, error) {
	hash, err := bee.GenerateFromPassword(data, cost)
	if err != nil {
		return nil, err
	}
	return hash, nil
}

// Compares an original hash with newly input raw data.
func HashCompare(original []byte, newRawData []byte) bool {
	err := bee.CompareHashAndPassword(original, newRawData)
	return err == nil
}

// Hashes a piece of data with a salt (default bcrypt cost) and Base 64-encodes it.
func HashBase64(data []byte) (*string, error) {
	return HashWithCostBase64(data, bee.DefaultCost)
}

// Hashes a piece of data with a salt and given cost and Base 64-encodes it.
func HashWithCostBase64(data []byte, cost int) (*string, error) {
	hash, err := HashWithCost(data, cost)
	if err != nil {
		return nil, err
	}

	encoded := base64.StdEncoding.EncodeToString(hash)
	return &encoded, nil
}

// Compares a hash with newly input raw data assuming they are Base 64-encoded.
func HashCompareBase64(original string, newRawData []byte) (bool, error) {
	originalBytes, err := base64.StdEncoding.DecodeString(original)
	if err != nil {
		return false, err
	}

	return HashCompare(originalBytes, newRawData), nil
}

// Hashes a piece of data with a salt (default bcrypt cost) and hex-encodes it.
func HashHex(data []byte) (*string, error) {
	return HashWithCostHex(data, bee.DefaultCost)
}

// Hashes a piece of data with a salt and given cost and hex-encodes it.
func HashWithCostHex(data []byte, cost int) (*string, error) {
	hash, err := HashWithCost(data, cost)
	if err != nil {
		return nil, err
	}

	var encoded = hex.EncodeToString(hash)
	return &encoded, nil
}

// Compares a hash with newly input raw data assuming they are hex-encoded.
func HashCompareHex(original string, newRawData []byte) (bool, error) {
	originalBytes, err := hex.DecodeString(original)
	if err != nil {
		return false, err
	}

	return HashCompare(originalBytes, newRawData), nil
}
