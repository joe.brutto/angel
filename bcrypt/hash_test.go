package bcrypt

import (
	"angel/random"
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	RandomStringLength = 2048
	EntropyIterations = 250
)

// Tests basic data hashing.
func TestHash(t *testing.T) {
	assertT := assert.New(t)

	toEncrypt := []byte(random.String(RandomStringLength))
	other := []byte(random.String(RandomStringLength))
	var alreadyGenerated [][]byte

	for index := 0; index < EntropyIterations; index++ {
		hash, err := Hash(toEncrypt)
		assertT.Nil(err, "Error generating hash")
		assertT.False(alreadyContains(alreadyGenerated, hash), "Generated the same hash multiple times!")

		alreadyGenerated = append(alreadyGenerated, hash)

		if index != 0 {
			assertT.True(HashCompare(hash, toEncrypt), "Hashes don't match, but they should have")
			assertT.False(HashCompare(hash, other), "Should have failed a check on an invalid entry")
		}
	}
}

// Tests data hashing with Base 64 encoding (entropy is tested elsewhere, so less iterations here).
func TestHashBase64(t *testing.T) {
	assertT := assert.New(t)

	toEncrypt := []byte(random.String(RandomStringLength))
	var alreadyGenerated []string

	for index := 0; index < 25; index++ {
		hash, err := HashBase64(toEncrypt)
		assertT.Nil(err, "Error generating hash")
		assertT.NotNil(hash, "Generated a nil hash (!?)")
		assertT.False(alreadyContainsString(alreadyGenerated, *hash), "Generated the same hash multiple times!")

		alreadyGenerated = append(alreadyGenerated, *hash)

		if index != 0 {
			result, err := HashCompareBase64(*hash, toEncrypt)
			assertT.Nil(err, "Error during Base 64 data comparison")
			assertT.True(result, "Hashes don't match, but they should have")
		}
	}
}

// Tests data hashing with hex encoding (entropy is tested elsewhere, so less iterations here).
func TestHashHex(t *testing.T) {
	assertT := assert.New(t)

	toEncrypt := []byte(random.String(RandomStringLength))
	var alreadyGenerated []string

	for index := 0; index < 25; index++ {
		hash, err := HashHex(toEncrypt)
		assertT.Nil(err, "Error generating hash")
		assertT.NotNil(hash, "Generated a nil hash (!?)")
		assertT.False(alreadyContainsString(alreadyGenerated, *hash), "Generated the same hash multiple times!")

		alreadyGenerated = append(alreadyGenerated, *hash)

		if index != 0 {
			result, err := HashCompareHex(*hash, toEncrypt)
			assertT.Nil(err, "Error during Base 64 data comparison")
			assertT.True(result, "Hashes don't match, but they should have")
		}
	}
}

// Utility function for determining if an array already contains a set of bytes.
func alreadyContains(haystack [][]byte, needle []byte) bool {
	for _, entry := range haystack {
		if bytes.Compare(entry, needle) == 0 {
			return true
		}
	}
	return false
}

// Utility function for determining if an array already contains a string.
func alreadyContainsString(haystack []string, needle string) bool {
	for _, entry := range haystack {
		if entry == needle {
			return true
		}
	}
	return false
}
