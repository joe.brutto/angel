package twofish

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
)

const (
	// https://www.schneier.com/academic/twofish/
	MinKeySize = 16
	MaxKeySize = 32
)

// Generates a key of the given size.
func GenerateKey(size int) ([]byte, error) {
	if size < MinKeySize || size > MaxKeySize {
		return nil, errors.New(fmt.Sprintf("key size must be [%d, %d], of which %d is not", MinKeySize, MaxKeySize, size))
	}

	keyBytes := make([]byte, size)
	_, err := rand.Read(keyBytes)
	return keyBytes, err
}

// Generates a key of a given size and Base-64 encodes it.
func GenerateKeyBase64(size int) (*string, error) {
	keyBytes, err := GenerateKey(size)
	if err != nil {
		return nil, err
	}

	encoded := base64.StdEncoding.EncodeToString(keyBytes)
	return &encoded, nil
}

// Generates a key of the given size and hex-encodes it.
func GenerateKeyHex(size int) (*string, error) {
	keyBytes, err := GenerateKey(size)
	if err != nil {
		return nil, err
	}

	encoded := hex.EncodeToString(keyBytes)
	return &encoded, nil
}

// Decodes a key byte slice from a Base 64-encoded string. This is really easy to do
// but I included it here just to be complete so that people can keep code in the
// same idiom as the rest of whatever they're doing. ¯\_(ツ)_/¯ I've been scolded for
// not doing stuff like this before.
func KeyFromBase64(key string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(key)
}

// Decodes a key byte slice from a hex-encoded string. This is really easy to do but
// I included it here just to be complete so that people can keep code in the same
// idiom as the rest of whatever they're doing. ¯\_(ツ)_/¯ I've been scolded for not
// doing stuff like this before.
func KeyFromHex(key string) ([]byte, error) {
	return hex.DecodeString(key)
}
