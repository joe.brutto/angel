package twofish

import (
	"crypto/rand"
	fish "golang.org/x/crypto/twofish"
)

// Generates an IV of the given byte size.
func GenerateIV() ([]byte, error) {
	data := make([]byte, fish.BlockSize)
	_, err := rand.Read(data)
	return data, err
}
