package twofish

import (
	"angel/encoding"
	"angel/utility"
	"bytes"
	"crypto/cipher"
	"errors"
	"fmt"
	fish "golang.org/x/crypto/twofish"
)

// Encrypts the given data using the provided key and the Blowfish algorithm. This
// assumes that we want the IV as the first 16 bytes of the encrypted data. Any
// errors we encounter are returned unless there's some kind of weird panic under
// the covers that we didn't expect to run across.
func CBCEncrypt(key []byte, data []byte) ([]byte, error) {
	// IV generation
	iv, err := GenerateIV()
	if err != nil {
		return nil, err
	}

	// encrypt
	block, err := fish.NewCipher(key)
	if err != nil {
		return nil, err
	}

	paddedContent := blowfishPadding(data)

	encrypted := make([]byte, len(paddedContent))
	ecb := cipher.NewCBCEncrypter(block, iv)
	ecb.CryptBlocks(encrypted, paddedContent)

	// shove the IV on the front of the byte array & exit
	return append(iv, encrypted...), nil
}

// Executes the decryption routine for the Blowfish algorithm. This assumes that the
// IV is the first 16 bytes of the encrypted data. Any errors we encounter are
// returned unless there's some kind of weird panic under the covers that we didn't
// expect to run across.
func CBCDecrypt(key []byte, data []byte) ([]byte, error) {
	// segregate the data
	ivBytes := data[:fish.BlockSize]
	toDecryptBytes := data[fish.BlockSize:]

	// verify the size of the data to the block
	if len(toDecryptBytes) % fish.BlockSize != 0 {
		return nil, errors.New(fmt.Sprintf("invalid encrypted data block size (%d)", len(toDecryptBytes)))
	}

	// decrypt
	block, err := fish.NewCipher(key)
	if err != nil {
		return nil, err
	}

	decrypted := make([]byte, len(toDecryptBytes))
	ecb := cipher.NewCBCDecrypter(block, ivBytes)
	ecb.CryptBlocks(decrypted, toDecryptBytes)

	// trim & exit
	return blowfishTrimming(decrypted), nil
}

// Executes the encryption routine for the Blowfish algorithm and then encodes it
// using the given routine.
func CBCEncryptEncoded(key []byte, data []byte, encode utility.EncodeBytesToString) (*string, error) {
	return utility.ExecuteWithEncoding(key, data, CBCEncrypt, encode)
}

// Executes the encryption routine for the Blowfish algorithm combination assuming
// that the data conforms to the given encoding.
func CBCDecryptEncoded(key []byte, data string, decode utility.DecodeBytesFromString) ([]byte, error) {
	return utility.ExecuteWithDecoding(key, data, CBCDecrypt, decode)
}

// Executes the encryption routine for the Blowfish algorithm and then Base
// 64-encodes the result. This assumes that we want the IV as the first bytes of the
// encrypted data.
func CBCEncryptBase64(key []byte, data []byte) (*string, error) {
	return CBCEncryptEncoded(key, data, encoding.EncodeBase64)
}

// Executes the decryption routine for the Blowfish combination assuming that the
// incoming data is Base 64-encoded. This also assumes that we have the IV as the
// first bytes of the encrypted data.
func CBCDecryptBase64(key []byte, data string) ([]byte, error) {
	return CBCDecryptEncoded(key, data, encoding.DecodeBase64)
}

// Executes the encryption routine for the Blowfish algorithm and then hex-encodes
// the result. This assumes that we want the IV as the first bytes of the encrypted
// data.
func CBCEncryptHex(key []byte, data []byte) (*string, error) {
	return CBCEncryptEncoded(key, data, encoding.EncodeHex)
}

// Executes the decryption routine for the Blowfish algorithm assuming that the
// incoming data is hex-encoded. This also assumes that we have the IV as the first
// bytes of the encrypted data.
func CBCDecryptHex(key []byte, data string) ([]byte, error) {
	return CBCDecryptEncoded(key, data, encoding.DecodeHex)
}

// Performs Blowfish standard padding.
func blowfishPadding(data []byte) []byte {
	modulus := len(data) % fish.BlockSize
	if modulus != 0 {
		paddingLength := fish.BlockSize - modulus
		for index := 0; index < paddingLength; index++ {
			data = append(data, 0)
		}
	}
	return data
}

// Performs Blowfish standard trimming.
func blowfishTrimming(data []byte) []byte {
	return bytes.Trim(data, "\x00")
}
