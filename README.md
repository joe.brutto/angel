# Angel

[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/joe.brutto/angel/-/blob/master/LICENSE)
[![Build Status](https://gitlab.com/joe.brutto/angel/badges/master/pipeline.svg)](https://gitlab.com/joe.brutto/angel/pipelines)
[![Coverage](https://gitlab.com/joe.brutto/angel/badges/master/coverage.svg)](https://gitlab.com/joe.brutto/angel/-/commits/master)

**A**nother **N**onsensical [**G**o](https://golang.org) **E**ncryption **L**ibrary.

Designed to take some headaches out of making encryption and decryption calls that I
commonly use in basic applications. Everything is condensed into simple, single
function calls in typical patterns I have seen and organized into sections based on
the algorithms being used rather than generic calls forcing you to know what you are
actually using in the application. This helps ensure compatibility between languages
that share the various encryption algorithms chosen to implement (see the library's
[sister project](https://gitlab.com/joe.brutto/shared-encryption) for the sandbox).

## Current TODOs

 * Support byte stream I/O along-side the in-memory buffer support already there
 * Add benchmarking tests
 * Add code coverage report
 * Not entirely happy with how the RSA stuff turned out on the first pass - make it better
 * Redesign the chained encryption to support a more diverse set of operations, not just simple key-based encryption

## Supported Algorithms

### One-Way 

 * [bcrypt](https://en.wikipedia.org/wiki/Bcrypt)

### Simple Keys

 * [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) / [CBC](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation) / [PKCS5PADDING](https://www.cryptosys.net/pki/manpki/pki_References.html#PKCS5)
 * [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) / [CBC](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation) / [PKCS7PADDING](https://www.cryptosys.net/pki/manpki/pki_Bibliography.html#PKCS7)
 * [Blowfish](https://en.wikipedia.org/wiki/Blowfish_(cipher)) (*with CBC*)
 
### Key Pairs

 * [RSA](https://simple.wikipedia.org/wiki/RSA_algorithm) Public/Private Keys

## Supported Patterns

 * Multi-encryption via chained calls

## Examples

TBD

## Disclaimers

 * No professional support is available
 * I tried to avoid any internal `panic` like I found in many other libraries and
   instead return the errors since I want to be able to more easily recover and
   report on errors
 * All contributions and suggestions are welcome as long as they are accompanied by
   unit tests, links to documentation on the algorithms, etc.
