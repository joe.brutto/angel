package rsa

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

// Writes the given key data to file. If you don't want a key written somewhere then
// just pass in a nil value to the path.
func (k *KeyPair) ToFiles(privateKeyPath *string, publicKeyPath *string) error {
	// private key
	err := k.writeToFile(privateKeyPath, k.PrivateKeyPemString)
	if err != nil {
		return err
	}

	// public key
	err = k.writeToFile(publicKeyPath, k.PublicKeyPemString)
	if err != nil {
		return err
	}

	return nil
}

// Loads a key pair from a pair of files. This assumes that there is no encoding
// applied to the files that we are reading the data from. If encoding is used then
// the specific encoding functions should be used in order to load the data from the
// files in the appropriate format.
//
// If you don't want a particular key loaded, just pass in nil to the argument.
func FromFiles(privateKeyFile *string, publicKeyFile *string, password[] byte) (*KeyPair, error) {
	privateKeyBytes, err := readFromFile(privateKeyFile)
	if err != nil {
		return nil, err
	}

	publicKeyBytes, err := readFromFile(publicKeyFile)
	if err != nil {
		return nil, err
	}

	return KeyPairFrom(privateKeyBytes, publicKeyBytes, password)
}

// Writes the data from the given conversion to file. If the file path given is nil
// the nothing will be written. Any errors are bubbled up, including if we do not
// have any data to actually write.
func (k *KeyPair) writeToFile(filePath *string, conversion conversionToFileFunction) error {
	if filePath != nil {
		data := conversion()
		if data == nil {
			return errors.New(fmt.Sprintf("no data to write to file \"%s\"", *filePath))
		}

		file, err := os.Create(*filePath)
		if err != nil {
			return err
		}

		defer func() {
			_ = file.Close()
		}()

		_, err = file.WriteString(*data)
		if err != nil {
			return err
		}
	}
	return nil
}

// Attempts to read back key data from file by loading all the bytes in the file.
func readFromFile(filePath *string) ([]byte, error) {
	if filePath != nil {
		return ioutil.ReadFile(*filePath)
	}
	return nil, nil
}
