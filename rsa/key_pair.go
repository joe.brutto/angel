package rsa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"errors"
)

// Simple struct allowing us to group together public/private key data. While the
// private key instance built in to Go contains the public key data we want to make
// the separation explicit/verbose.
type KeyPair struct {
	PrivateKey *rsa.PrivateKey
	PublicKey *rsa.PublicKey
}

// Function definitions for converting data to/from file
type conversionToFileFunction func() *string

// Generates a key pair of the given bit length.
func GenerateKeyPair(bits int) (*KeyPair, error) {
	// TODO: add support for a password
	// TODO: switch this to use the CyryptoSource-based NewRandom when we figure out how to make a reader

	// basic key generation
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return nil, err
	}

	// return the structured data
	return &KeyPair{
		PrivateKey: privateKey,
		PublicKey:  &privateKey.PublicKey,
	}, nil
}

// Converts a slice of bytes into an rsa.PrivateKey. If the private key is encrypted
// then we will use the given password to decrypt it. Remember that you can have an
// encrypted key without a password and we are able to deal with the particular
// scenario as well.
func ToPrivateKey(bytes []byte, password []byte) (*rsa.PrivateKey, error) {
	// basic setup
	block, _ := pem.Decode(bytes)
	blockBytes := block.Bytes

	// deal with encrypted blocks
	if x509.IsEncryptedPEMBlock(block) {
		decrypted, err := x509.DecryptPEMBlock(block, password)
		if err != nil {
			return nil, err
		}
		blockBytes = decrypted
	}

	// parse
	key, err := x509.ParsePKCS1PrivateKey(blockBytes)
	if err != nil {
		return nil, err
	}

	// return the loaded key
	return key, nil
}

// Converts a slice of bytes into an rsa.PublicKey. If the public key is encrypted
// then we will use the given password to decrypt it. Remember that you can have an
// encrypted key without a password and we are able to deal with the particular
// scenario as well.
func ToPublicKey(bytes []byte, password []byte) (*rsa.PublicKey, error) {
	// basic setup
	block, _ := pem.Decode(bytes)
	blockBytes := block.Bytes

	// deal with encrypted blocks
	if x509.IsEncryptedPEMBlock(block) {
		decrypted, err := x509.DecryptPEMBlock(block, password)
		if err != nil {
			return nil, err
		}
		blockBytes = decrypted
	}

	// parse
	return x509.ParsePKCS1PublicKey(blockBytes)
}

// Loads a KeyPair from the given set of bytes (either can be nil).
func KeyPairFrom(privateKeyBytes []byte, publicKeyBytes []byte, password []byte) (*KeyPair, error) {
	var privateKey *rsa.PrivateKey
	var publicKey *rsa.PublicKey

	// private key
	if privateKeyBytes != nil {
		privateKeyLoaded, err := ToPrivateKey(privateKeyBytes, password)
		if err != nil {
			return nil, err
		}
		privateKey = privateKeyLoaded
	}

	// public key
	if publicKeyBytes != nil {
		publicKeyLoaded, err := ToPublicKey(publicKeyBytes, password)
		if err != nil {
			return nil, err
		}
		publicKey = publicKeyLoaded
	}

	// return the loaded data
	return &KeyPair{PrivateKey: privateKey, PublicKey: publicKey}, nil
}

// Converts the currently set private key to bytes.
func (k *KeyPair) PrivateKeyBytes() ([]byte, error) {
	var bytes []byte
	if k.PrivateKey != nil {
		x509Bytes := x509.MarshalPKCS1PrivateKey(k.PrivateKey)
		bytes = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509Bytes})
	}
	return bytes, nil
}

// Converts the currently set public key to bytes.
func (k *KeyPair) PublicKeyBytes() ([]byte, error) {
	var bytes []byte
	if k.PublicKey != nil {
		asn1 := x509.MarshalPKCS1PublicKey(k.PublicKey)
		bytes = pem.EncodeToMemory(&pem.Block{Type: "RSA PUBLIC KEY", Bytes: asn1})
	}
	return bytes, nil
}

// Converts the PrivateKey to a string.
func (k *KeyPair) PrivateKeyPemString() *string {
	var converted *string
	if k.PrivateKey != nil {
		encoded := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k.PrivateKey)})
		encodedString := string(encoded)
		converted = &encodedString
	}
	return converted
}

// Converts the PublicKey to a string.
func (k *KeyPair) PublicKeyPemString() *string {
	var converted *string
	if k.PublicKey != nil {
		encoded := pem.EncodeToMemory(&pem.Block{Type: "RSA PUBLIC KEY", Bytes: x509.MarshalPKCS1PublicKey(k.PublicKey)})
		encodedString := string(encoded)
		converted = &encodedString
	}
	return converted
}

// Encrypts the give data using the public key.
func (k *KeyPair) Encrypt(data []byte) ([]byte, error) {
	if k.PublicKey == nil {
		return nil, errors.New("no public key provided for encryption")
	}

	hash := sha256.New()
	return rsa.EncryptOAEP(hash, rand.Reader, k.PublicKey, data, []byte{})
}

// Decrypts the given data using the private key.
func (k *KeyPair) Decrypt(data []byte) ([]byte, error) {
	if k.PrivateKey == nil {
		return nil, errors.New("no private key provided for encryption")
	}

	hash := sha256.New()
	return rsa.DecryptOAEP(hash, rand.Reader, k.PrivateKey, data, []byte{})
}
