package rsa

import (
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
)

// Tests reading and writing of files for keys.
func TestToFromFiles(t *testing.T) {
	assertT := assert.New(t)

	// generate a key pair to work with
	keyPair, err := GenerateKeyPair(KeyPairSizes[0])
	assertT.Nil(err, "Unable to generate key pair")
	assertT.NotNil(keyPair, "Generated a nil key pair (!?)")

	// where are we storing the files?
	privateKeyPath := "./test-private.key"
	publicKeyPath := "./test-public.key"

	defer deleteFile(privateKeyPath)
	defer deleteFile(publicKeyPath)

	// write the files
	err = keyPair.ToFiles(&privateKeyPath, &publicKeyPath)
	assertT.Nilf(err, "Error writing key pair to file: %v", err)

	// read the files back in
	reloadedKeyPair, err := FromFiles(&privateKeyPath, &publicKeyPath, nil)
	assertT.Nilf(err, "Unable to reload keys from file: %v", err)
	assertT.NotNil(reloadedKeyPair, "Got back a nil key pair (!?)")

	// make sure the content matches
	assertT.Equal(keyPair.PrivateKey, reloadedKeyPair.PrivateKey, "Private key data mismatch")
	assertT.Equal(keyPair.PublicKey, reloadedKeyPair.PublicKey, "Public key data mismatch")
}

// Deletes a file from the file system if it exists.
func deleteFile(filePath string) {
	info, err := os.Stat(filePath)
	if !os.IsNotExist(err) {
		if !info.IsDir() {
			err := os.Remove(filePath)
			if err != nil {
				log.Printf("Error removing file: %v", err)
			}
		}
	}
}
