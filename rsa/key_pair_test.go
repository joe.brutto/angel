package rsa

import (
	"angel/random"
	"github.com/stretchr/testify/assert"
	"testing"
)

const TestStringSize = 128
var KeyPairSizes = [...]int{2048, 4096}

// Tests the basic generation of key pair data.
func TestGenerateKeyPair(t *testing.T) {
	assertT := assert.New(t)

	// basic data generation and data check
	for _, keyPairSize := range KeyPairSizes {
		keyPair, err := GenerateKeyPair(keyPairSize)
		assertT.Nil(err, "Error generating key pair")
		assertT.NotNil(keyPair, "Generated a nil KeyPair (!?)")
	}
}

// Tests converting bytes to a key pair from a generated key pair set.
func TestKeyPairFrom(t *testing.T) {
	assertT := assert.New(t)

	for _, keyPairSize := range KeyPairSizes {
		keyPair, err := GenerateKeyPair(keyPairSize)
		assertT.Nil(err, "Error generating key pair")
		assertT.NotNil(keyPair, "Generated a nil key pair (!?)")

		privateKeyBytes, err := keyPair.PrivateKeyBytes()
		assertT.Nil(err, "Error converting private key to bytes")
		assertT.NotNil(privateKeyBytes, "Generated nil bytes for the private key (!?)")

		publicKeyBytes, err := keyPair.PublicKeyBytes()
		assertT.Nil(err, "Error converting public key to bytes")
		assertT.NotNil(publicKeyBytes, "Generated nil bytes for the public key (!?)")

		loaded, err := KeyPairFrom(privateKeyBytes, publicKeyBytes, nil)
		assertT.Nil(err, "Error reloading keys from bytes")
		assertT.NotNil(loaded, "Generated a nil KeyPair on reload (!?)")
	}
}

// Tests basic data encryption. This oly checks for errors as the decryption routine
// is tested in cooperation with this elsewhere in our tests.
func TestKeyPair_Encrypt(t *testing.T) {
	assertT := assert.New(t)

	for _, keyPairSize := range KeyPairSizes {
		data := random.String(TestStringSize)

		keyPair, err := GenerateKeyPair(keyPairSize)
		assertT.Nil(err, "Error generating key pair")
		assertT.NotNil(keyPair, "Generated a nil key pair (!?)")

		result, err := keyPair.Encrypt([]byte(data))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(result, "Generated a nil encryption result (!?)")
	}
}

// Tests basic data encryption/decryption.
func TestKeyPair_Decrypt(t *testing.T) {
	assertT := assert.New(t)

	for _, keyPairSize := range KeyPairSizes {
		data := random.String(TestStringSize)

		keyPair, err := GenerateKeyPair(keyPairSize)
		assertT.Nil(err, "Error generating key pair")
		assertT.NotNil(keyPair, "Generated a nil key pair (!?)")

		encrypted, err := keyPair.Encrypt([]byte(data))
		assertT.Nil(err, "Error encrypting data")
		assertT.NotNil(encrypted, "Generated a nil encryption result (!?)")

		decrypted, err := keyPair.Decrypt(encrypted)
		assertT.Nil(err, "Error decrypting data")
		assertT.NotNil(decrypted, "Generated a nil decryption result (!?)")
		assertT.Equal(data, string(decrypted), "Invalid data decrypted")
	}
}
