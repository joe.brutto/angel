package random

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Tests the generation of random bytes.
func TestBytes(t *testing.T) {
	assertT := assert.New(t)

	for index := 16; index < 4096; index *= 2 {
		result, err := Bytes(index)
		assertT.Nil(err, "Error generating random byte slice")
		assertT.Equal(index, len(result), "Invalid number of bytes generated")
	}
}
