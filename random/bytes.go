package random

// Generates a random byte array of the given length.
func Bytes(length int) ([]byte, error) {
	buffer := make([]byte, length)
	_, err := NewRandom().Read(buffer)
	if err != nil {
		return nil, err
	}
	return buffer, nil
}
