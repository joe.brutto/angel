package random

import (
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

// Tests generation of strings with character set limitations.
func TestStringWithCharset(t *testing.T) {
	assertT := assert.New(t)

	runes := "zaphod456"
	var result string
	for index := 4; index < 1024; index++ {
		result = StringWithCharset(index, runes)
		assertT.Equal(index, len(result), "Generated string of invalid length")

		matched, err := regexp.MatchString(`^[zaphod456]+$`, result)
		assertT.Nil(err, "Error comparing generated string with regex")
		assertT.Truef(matched, "Generated a string with invalid characters: \"%s\"", result)
	}
}

// Tests generation of alphanumeric strings.
func TestStringAlphanumeric(t *testing.T) {
	assertT := assert.New(t)

	var result string
	for index := 4; index < 1024; index++ {
		result = StringAlphanumeric(index)
		assertT.Equal(index, len(result), "Generated string of invalid length")

		matched, err := regexp.MatchString(`(?i)^[a-z0-9]+$`, result)
		assertT.Nil(err, "Error comparing generated string with regex")
		assertT.Truef(matched, "Generated a string with invalid characters: \"%s\"", result)
	}
}

// Tests generation of random strings
func TestString(t *testing.T) {
	assertT := assert.New(t)

	var result string
	for index := 4; index < 1024; index++ {
		result = String(index)
		assertT.Equal(index, len(result), "Generated string of invalid length")
	}
}
