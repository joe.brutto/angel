package random

import (
	cryptoRandom "crypto/rand"
	"encoding/binary"
	"log"
	"math/rand"
	"time"
)

// Generates a new secure random instance using key library standards.
func NewRandom() *rand.Rand {
	var source CryptoSource
	return rand.New(source)
}

// Custom type allowing for generating of securely randomized data. Rather than just
// log an error if something happens we attempt to recover as best as we can but add
// a log messages that you should monitor for and alert on in your logging stack
// with whatever priority is important to your organization.
//
// Credit: https://yourbasic.org/golang/crypto-rand-int/
type CryptoSource struct {}

func (s CryptoSource) Seed(_ int64) {}

func (s CryptoSource) Int63() int64 {
	return int64(s.Uint64() & ^uint64(1 << 63))
}

func (s CryptoSource) Uint64() uint64 {
	var value uint64
	err := binary.Read(cryptoRandom.Reader, binary.BigEndian, &value)
	if err != nil {
		// TODO: need to find a better way to handle this :-/

		// log the error
		log.Printf("Error doing BigEndian read for secure random string: %v", err)

		// seed and use a default
		rand.Seed(time.Now().UTC().UnixNano())
		value = rand.Uint64()
	}
	return value
}
