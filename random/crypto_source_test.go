package random

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Tests the generation of new securely seeded Random instances.
func TestNewRandom(t *testing.T) {
	assertT := assert.New(t)

	random := NewRandom()
	assertT.NotNil(random, "Generated a nil Random instance (!?)")
}
