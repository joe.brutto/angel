package random

const (
	RunesAlphanumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	RunesSpecial = ",./<>?;':\"[]{}\\|-=_+`~!@#$%^&*()"
)

// Generates a random string of the specified length using only the runes in the
// given character set.
func StringWithCharset(length int, characterSet string) string {
	random := NewRandom()
	buffer := make([]byte, length)
	for index := range buffer {
		buffer[index] = characterSet[random.Intn(len(characterSet))]
	}
	return string(buffer)
}

// Generates a random alphanumeric string of the specified length.
func StringAlphanumeric(length int) string {
	return StringWithCharset(length, RunesAlphanumeric)
}

// Generates a random string of the specified length.
func String(length int) string {
	return StringWithCharset(length, RunesAlphanumeric + RunesSpecial)
}
